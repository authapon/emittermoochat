package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	emitter "github.com/emitter-io/go/v2"
)

var (
	username = ""
	key      = ""
	channel  = ""
	broker   = ""
)

func main() {

	if len(os.Args) < 5 {
		fmt.Println("Usage:")
		fmt.Println("\temittermoochat <key> <channel> <broker> [<username> || console]")
		os.Exit(0)
	}

	key = os.Args[1]
	channel = os.Args[2]
	broker = os.Args[3]
	username = os.Args[4]

	c, err := emitter.Connect(broker, nil)
	if err != nil {
		panic(err)
	}
	switch username {
	case "console":
		c.Subscribe(key, channel, func(_ *emitter.Client, msg emitter.Message) {
			fmt.Println(string(msg.Payload()))
		})
		select {}
	default:
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			c.Publish(key, channel, username+": "+strings.TrimSpace(scanner.Text()))
		}
	}
}
